import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";

import { Observable, throwError } from 'rxjs';

import { environment } from '@environment/environment';

import { Character } from '@shared/interface/character.interface';
import { TrackHttpError } from '@shared/models/trackHttpError';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(private http: HttpClient) { }

  // Method for searching characters
  searchCharacters(query = '', page = 1): Observable<Character[] | TrackHttpError>{

    const filter = `${environment.baseUrlAPI}/?name=${query}&page=${page}`;
    return this.http.get<Character[]>(filter)
    .pipe(catchError((err) => this.handleHttpError(err)));
  }

  // Method for character details
  getDetails(id: number) {
    return this.http.get<Character>(`${environment.baseUrlAPI}/${id}`);
  }

  private handleHttpError(error: HttpErrorResponse): Observable<TrackHttpError>{
    let dataError = new TrackHttpError();
    dataError.errorNumber = error.status;
    dataError.message = error.statusText;
    dataError.friendlyMessage = 'An error ocurred retrieving data';
    return throwError(dataError);
  }
}

import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { ActivatedRoute, ParamMap, NavigationEnd, Router } from '@angular/router';
import { DOCUMENT } from "@angular/common";

import { take, filter } from "rxjs/operators";

import { Character } from '@app/shared/interface/character.interface';
import { CharacterService } from '@app/shared/services/character.service';
import { TrackHttpError } from '@app/shared/models/trackHttpError';


type RequestInfo = {
  next: string;
};
@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.scss']
})
export class CharacterListComponent implements OnInit {

  characters: Character[] = [];
  info: RequestInfo = {
    next: null
  };
  showGoUpButton = false;
  private pageNum=1;
  private query: string;
  private hidescrollHeight = 200;
  private showScrollHeight = 200;

  constructor(
              @Inject(DOCUMENT) private document: Document,
              private characterService: CharacterService,
              private route: ActivatedRoute,
              private router: Router) {
                this.onUrlChanged();
              }

  ngOnInit(): void {
    // this.getDatafromService();
    this.getCharactersByQuery();
  }

  @HostListener('window:scroll', [])
  onWindowScroll(): void{
    const yOffset = window.pageYOffset;
    if ((yOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop) > this.showScrollHeight) {
      this.showGoUpButton = true;
    }
    else if (this.showGoUpButton && (yOffset || this.document.documentElement.scrollTo || this.document.body.scrollTop)) {
      this.showGoUpButton = false;
    }
  }

  onScrollDown(): void{
   if (this.info.next) {
     this.pageNum++;
     this.getDatafromService();
   }
  }
  onScrollTop(): void{
    this.document.body.scrollTop = 0; //Safari Browser
    this.document.documentElement.scrollTop = 0; // Other Browsers
  }

  private onUrlChanged(): void {
    this.router.events
    .pipe(filter((event) => event instanceof NavigationEnd))
    .subscribe(() => {
      this.characters = [];
      this.pageNum = 1;
      this.getCharactersByQuery();
    })
  }

  private getCharactersByQuery(): void {
    this.route.queryParams.pipe(
      take(1)).subscribe((params: ParamMap) => {
        this.query = params['q'];
        this.getDatafromService();
      });
  }

  private getDatafromService(): void {
    this.characterService.searchCharacters(this.query, this.pageNum)
    .pipe(take(1))
    .subscribe((res: any) => {
      console.log('Response->', res);

      if (res?.results?.length) {
        const { info, results } = res;
      this.characters = [...this.characters, ...results];
      this.info = info;
      }
      else{
        this.characters = [];
      }
    },
    (error:TrackHttpError)=> console.log((error.friendlyMessage)));
  }
}
